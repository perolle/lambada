<?php
    include('modules/partie1.php');
?>
<?php
// Import et instanciation de la classe Database
require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

// Trouver le chiffre de la semaine courante(actuelle)
$weekNumber = date("W");
//var_dump($weekNumber);
$month = date("F");

// Chercher les séances de cette semaine
$seancesOfWeek = $database->getSeanceByWeek($weekNumber);
$weekOfMounth = $database->getWeekByMonth($monthNumber);

// On crée les index associés au jour
const LUNDI = 1;
const MARDI = 2;
const MERCREDI = 3;
const JEUDI = 4;
const VENDREDI = 5;
const SAMEDI = 6;

const WEEK1 = 1;
const WEEK2 = 2;
const WEEK3 = 3;
const WEEK4 = 4;
// Ranger les séances par jour de la semaine en commencant le lundi
$seances = [];
$seances[WEEK1] = [];
$seances[WEEK2] = [];
$seances[WEEK3] = [];
$seances[WEEK4] = [];

$seances[LUNDI] = [];
$seances[MARDI] = [];
$seances[MERCREDI] = [];
$seances[JEUDI] = [];
$seances[VENDREDI] = [];
$seances[SAMEDI] = [];

foreach($seancesOfWeek as $seance){
    // on détermine le jour de la séance
    $indexDay = date("w", strtotime($seance->getDate()));
    // On ajoute la séance dans un tableau associé au numéro du jour de la semaine
    array_push($seances[$indexDay], $seance);
}
?>
<div id="plan">

    <div class="container card text-center mt-4">
        <h1 class="card-header"><?php echo $month;?></h1>
        <p class="plantxt">Planning des cours du mois</p>
        <div class="card-body">
            <div class="row">
            <div id="lundi" class="col-6 col-md-4 col-lg-2 tour">
                    <h3>LUNDI</h3>
                    <?php
                        foreach($seances[LUNDI] as $seance){
                            include('modules/etiquettes.php');
                        }
                    ?>
                </div>
                <div id="mardi" class="col-6 col-md-4 col-lg-2 tour">
                    <h3>MARDI</h3>
                    <?php
                        foreach($seances[MARDI] as $seance){
                            include('modules/etiquettes.php');
                        }
                    ?>
                </div>
                <div id="mercredi" class="col-6 col-md-4 col-lg-2 tour">
                    <h3>MERCREDI</h3>
                    <?php
                        foreach($seances[MERCREDI] as $seance){
                            include('modules/etiquettes.php');
                        }
                    ?>
                </div>
                <div id="jeudi" class="col-6 col-md-4 col-lg-2 tour">
                    <h3>JEUDI</h3>
                    <?php
                        foreach($seances[JEUDI] as $seance){
                            include('modules/etiquettes.php');
                        }
                    ?>
                </div>
                <div id="vendredi" class="col-6 col-md-4 col-lg-2 tour">
                    <h3>VENDREDI</h3>
                    <?php
                        foreach($seances[VENDREDI] as $seance){
                            include('modules/etiquettes.php');
                        }
                    ?>
                </div>
                <div id="samedi" class="col-6 col-md-4 col-lg-2 tour">
                    <h3>SAMEDI</h3>
                    <?php
                        foreach($seances[SAMEDI] as $seance){
                            include('modules/etiquettes.php');
                        }
                    ?>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php
    include('modules/partie3.php');
?>