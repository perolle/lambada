<?php
    include('modules/partie1.php');
?>
<div id="pageone">
    <div class="container card text-center mt-4 mb-5">
        <h1 class="card-header"><img src="/vues/assets/img/LSC.png" alt="logo" class="w-50">Lambada Sport Club</h1>
        <div class="card-body">
            <img class="mainImage w-100" src="/vues/assets/img/fit.png">
            <p class="p-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore 
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <div class="button_cont" align="center"><a class="bouton" href="/vues/planning.php" target="_blank" rel="nofollow noopener">Consulter le planning</a></div>
        </div>
    </div>
</div>
<?php
    include('modules/partie3.php');
?>