<?php
// on importe la classe User
require_once(__DIR__ ."/../../models/User.php");
// on récupère le user dans la session
$user = isset($_SESSION["user"]) ? unserialize($_SESSION["user"]) : null;

?>
<div id="navbar">
<nav class="navbar navbar-expand-lg navbar-light">

  <a class="navbar-brand ml-5" href="/vues/index.php"><img src="/vues/assets/img/LSC.png" alt="logo" class="w-50"></a>

  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto mr-5">
      <?php if($user == null){ ?>
        <li class="nav-item">
          <a class="nav-link" href="/vues/login.php">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/vues/inscription.php">Inscription</a>
        </li>
      <?php }//endif ?>
      <li class="nav-item">
        <a class="nav-link" href="/vues/planning.php">Planning</a>
      </li>
      <?php if($user != null){ ?>
        <?php if($user->isAdmin() == 1){ ?>
          <li class="nav-item">
            <a class="nav-link" href="/vues/formulaire.php">Créer cours</a>
          </li>
        <?php } //endif ?>
        <li class="nav-item">
          <a class="nav-link" href="/vues/profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/process/deconnexion.php">Déconnexion</a>
        </li>
      <?php }//endif ?>
    </ul>
  </div>
</nav>
</div>