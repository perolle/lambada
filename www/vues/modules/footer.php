
<div id="footer">
    <footer>
        <div class="container-fluid  pt-2">
            <div class="row text-center text-xs-center text-sm-left text-md-left">
                <div class="col-xs-12 col-sm-4 col-md-3 box bg-warning text-white p-3">
                    <h5>Relaxation</h5>
                    <ul class="list-unstyled text-white">
                       
                        <li><a href="#">présentation</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Get Started</a></li>
                        <li><a href="#">Videos</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 box bg-danger text-white p-3">
                    <h5>Fitness</h5>
                    <ul class="list-unstyled text-white">
                        
                        <li><a href="#">présentation</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Get Started</a></li>
                        <li><a href="#">Videos</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 box bg-info text-white p-3">
                    <h5>Endurance</h5>
                    <ul class="list-unstyled text-white">
                       
                        <li><a href="#">présentation</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Get Started</a></li>
                        <li><a href="#">Videos</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 box bg-success text-white p-3">
                    <h5>Diététique</h5>
                    <ul class="list-unstyled text-white">
                    
                        <li><a href="#">présentation</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Get Started</a></li>
                        <li><a href="#">Videos</a></li>
                    </ul>
                </div>
            </div>	
            <div class="row copyright">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center badge badge-dark">
                    <p class="h6 mt-3"><a href="/vues/index.php">return home</a></p>
                    <p class="h6">&copy All right Reserved. <a class="ml-2" href="https://www.sandravuagniaux" target="_blank">Sandra Vuagniaux</a></p>
                    
                </div>
                </hr>
            </div>	
        </div>
    </footer>
</div>